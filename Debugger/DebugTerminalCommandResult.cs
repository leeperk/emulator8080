﻿namespace Debugger
{
    public enum DebugTerminalCommandResult
    {
        ExitDebugTerminal,
        ExecuteOneInstruction,
        RunUntilNextReturn,
        ContinueDebugTerminal
    }
}
