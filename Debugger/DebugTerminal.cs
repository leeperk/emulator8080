﻿using Cpu8080;
using Cpu8080.Components;
using Memory;
using System;
using System.Globalization;

namespace Debugger
{
    public class DebugTerminal
    {
        private readonly Cpu _cpu;
        private readonly IMemoryManager _memory;
        private readonly Disassembler _disassembler = new Disassembler();

        private DebugTerminalCommandResult _commandStateCurrentlyRunning = DebugTerminalCommandResult.ContinueDebugTerminal;
        private UInt16 _returnAddressForRunUntilNextReturn;

        public DebugTerminal(Cpu cpu, IMemoryManager memory)
        {
            _cpu = cpu;
            _memory = memory;
        }

        public DebugTerminalCommandResult OpenDebugTerminal()
        {
            var status = DebugTerminalCommandResult.ContinueDebugTerminal;
            while (status == DebugTerminalCommandResult.ContinueDebugTerminal)
            {
                Console.WriteLine(_disassembler.GenerateStateForDisplay(_cpu.State, _memory));
                Console.WriteLine(_disassembler.GenerateDisassembledInstructionForDisplay(_memory, _cpu.State.PC));
                if (_commandStateCurrentlyRunning == DebugTerminalCommandResult.RunUntilNextReturn)
                {
                    if (_cpu.State.PC == _returnAddressForRunUntilNextReturn)
                    {
                        _commandStateCurrentlyRunning = DebugTerminalCommandResult.ContinueDebugTerminal;
                    }
                    else
                    {
                        return _commandStateCurrentlyRunning;
                    }
                }
                Console.Write("> ");
                var command = Console.ReadLine();
                status = ProcessCommand(command);
            }
            _commandStateCurrentlyRunning = status;
            return status;
        }

        private DebugTerminalCommandResult ProcessCommand(string command)
        {
            var parts = command.ToLowerInvariant().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 0)
            {
                switch (parts[0])
                {
                    case "e":
                        return DebugTerminalCommandResult.ExecuteOneInstruction;
                    case "o":
                        {
                            var sizeOfInstruction = _disassembler.GetSizeInBytesOfInstructionByOpCode(_memory.Read_8(_cpu.State.PC));
                            _returnAddressForRunUntilNextReturn = (UInt16)(_cpu.State.PC + sizeOfInstruction);
                            return DebugTerminalCommandResult.RunUntilNextReturn;
                        }
                    case "q":
                        return DebugTerminalCommandResult.ExitDebugTerminal;
                    case "dm":
                        if (parts.Length > 1)
                        {
                            UInt16 address;
                            if (UInt16.TryParse(parts[1], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out address))
                            {
                                DumpMemory(address);
                            }
                        }
                        break;
                    case "di":
                        _disassembler.GenerateDisassembledInstructionForDisplay(_memory, _cpu.State.PC);
                        Console.WriteLine();
                        break;
                    case "int":
                        if (parts.Length > 1)
                        {
                            if (parts[1] == "off")
                            {
                                _cpu.State.InterruptsEnabled = 0;
                                Console.WriteLine("Interrupts disabled");
                            }
                            else if (parts[1] == "on")
                            {
                                _cpu.State.InterruptsEnabled = 1;
                                Console.WriteLine("Interrupts ensabled");
                            }
                        }
                        else
                        {
                            Console.WriteLine($"Interrupts are currently {(_cpu.State.InterruptsEnabled == 0 ? "disabled" : "enabled")}");
                        }

                        break;
                    case "ds":
                        _disassembler.GenerateStateForDisplay(_cpu.State, _memory);
                        break;
                }
            }
            return DebugTerminalCommandResult.ContinueDebugTerminal;
        }

        private void DumpMemory(UInt16 address)
        {
            int dumpAddress = address;
            var countOnLine = 0;
            while (dumpAddress < address + 0x50 && dumpAddress <= 0x3FFF)
            {
                if (countOnLine == 0)
                {
                    Console.Write($"{dumpAddress:X4}  ");
                }

                Console.Write($"{_memory.Read_8(dumpAddress):X2} ");
                countOnLine++;

                if (countOnLine == 8)
                {
                    Console.Write(" ");
                }
                else if (countOnLine == 16)
                {
                    countOnLine = 0;
                    Console.WriteLine();
                }
                dumpAddress++;
            }
            if (countOnLine != 0)
            {
                Console.WriteLine();
            }
        }
    }
}
