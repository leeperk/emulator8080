﻿using Cpu8080.Components;
using Memory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Cpu8080
{
    public class Disassembler
    {
        public string GenerateDisassembledInstructionForDisplay(IMemoryManager memory, UInt16 pc)
        {
            if (pc >= 0x4000)
            {
                return "XXX";
            }
            var (mnemonic, parameterFormat) = _opCodeDefinitions[memory.Read_8(pc)];
            return FormatDisassembledLine(memory, pc, mnemonic, parameterFormat);
        }

        public string GenerateStateForDisplay(CpuState state, IMemoryManager memory)
        {
            var stateLine = new StringBuilder();

            stateLine.Append($"{((state.Flags & CpuState.SignFlagMask) > 0 ? "s" : ".")}");
            stateLine.Append($"{((state.Flags & CpuState.ZeroFlagMask) > 0 ? "z" : ".")}");
            stateLine.Append(".");
            stateLine.Append($"{((state.Flags & CpuState.HalfCarryFlagMask) > 0 ? "a" : ".")}");

            stateLine.Append(".");
            stateLine.Append($"{((state.Flags & CpuState.ParityFlagMask) > 0 ? "p" : ".")}");
            stateLine.Append(".");
            stateLine.Append($"{((state.Flags & CpuState.CarryFlagMask) > 0 ? "c" : ".")}");
            UInt16 hlAddress = (UInt16)(state.H << 8 | state.L);
            var displayHLByte = hlAddress < 16 * 1024 ? $"{memory.Read_8(hlAddress):X2}" : "..";
            stateLine.Append($"  A-{state.A:X2} B-{state.B:X2} C-{state.C:X2} DE-{state.D:X2}{state.E:X2} HL-{state.H:X2}{state.L:X2}({displayHLByte}) SP-{state.SP:X4}");

            stateLine.Append($"(");
            UInt16 stack = state.SP;
            while (stack < 16 * 1024 && stack < state.SP + 5)
            {
                stateLine.Append($"{memory.Read_8(stack++):X2} ");
            }
            if (stack != state.SP)
            {
                stateLine.Remove(stateLine.Length - 1, 1);
            }
            stateLine.Append($")");
            return stateLine.ToString();
        }

        public byte GetSizeInBytesOfInstructionByOpCode(byte opCode)
        {
            return GetParameterCountFromParameterFormat(_opCodeDefinitions[opCode].ParameterFormat);
        }

        private Dictionary<byte, (string Mnemonic, string ParameterFormat)> _opCodeDefinitions = new Dictionary<byte, (string, string)>()
        {
            { OpCodes.x00_NOP,         ("NOP", "") },
            { OpCodes.x01_LXI_B_word,  ("LXI", "B,#${1:X2}{0:X2}") },
            { OpCodes.x02_STAX_B,      ("STAX", "B") },
            { OpCodes.x03_INX_B,       ("INX", "B") },
            { OpCodes.x04_INR_B,       ("INR", "B") },
            { OpCodes.x05_DCR_B,       ("DCR", "B") },
            { OpCodes.x06_MVI_B_byte,  ("MVI", "B,#${0:X2}") },
            { OpCodes.x07_RLC,         ("RLC", "") },
            { OpCodes.x08_unknown,     ("???", "") },
            { OpCodes.x09_DAD_B,       ("DAD", "B") },
            { OpCodes.x0a_LDAX_B,      ("LDAX", "B") },
            { OpCodes.x0b_DCX_B,       ("DCX", "B") },
            { OpCodes.x0c_INR_C,       ("INR", "C") },
            { OpCodes.x0d_DCR_C,       ("DCR", "C") },
            { OpCodes.x0e_MVI_C_byte,  ("MVI", "C,#${0:X2}") },
            { OpCodes.x0f_RRC,         ("RRC", "") },
            { OpCodes.x10_unknown,     ("???", "") },
            { OpCodes.x11_LXI_D_word,  ("LXI", "D,#${1:X2}{0:X2}") },
            { OpCodes.x12_STAX_D,      ("STAX", "D") },
            { OpCodes.x13_INX_D,       ("INX", "D") },
            { OpCodes.x14_INR_D,       ("INR", "D") },
            { OpCodes.x15_DCR_D,       ("DCR", "D") },
            { OpCodes.x16_MVI_D_byte,  ("MVI", "D,#${0:X2}") },
            { OpCodes.x17_RAL,         ("RAL", "") },
            { OpCodes.x18_unknown,     ("???", "") },
            { OpCodes.x19_DAD_D,       ("DAD", "D") },
            { OpCodes.x1a_LDAX_D,      ("LDAX", "D") },
            { OpCodes.x1b_DCX_D,       ("DCX", "D") },
            { OpCodes.x1c_INR_E,       ("INR", "E") },
            { OpCodes.x1d_DCR_E,       ("DCR", "E") },
            { OpCodes.x1e_MVI_E_byte,  ("MVI", "E,#${0:X2}") },
            { OpCodes.x1f_RAR,         ("RAR", "") },
            { OpCodes.x20_unknown,     ("???", "") },
            { OpCodes.x21_LXI_H_word,  ("LXI", "H,#${1:X2}{0:X2}") },
            { OpCodes.x22_SHLD_addr,   ("SHLD", "${1:X2}{0:X2}") },
            { OpCodes.x23_INX_H,       ("INX", "H") },
            { OpCodes.x24_INR_H,       ("INR", "H") },
            { OpCodes.x25_DCR_H,       ("DCR", "H") },
            { OpCodes.x26_MVI_H_byte,  ("MVI", "H,#${0:X2}") },
            { OpCodes.x27_DAA,         ("DAA", "") },
            { OpCodes.x28_unknown,     ("???", "") },
            { OpCodes.x29_DAD_H,       ("DAD", "H") },
            { OpCodes.x2a_LHLD_addr,   ("LHLD", "${1:X2}{0:X2}") },
            { OpCodes.x2b_DCX_H,       ("DCX", "H") },
            { OpCodes.x2c_INR_L,       ("INR", "L") },
            { OpCodes.x2d_DCR_L,       ("DCR", "L") },
            { OpCodes.x2e_MVI_L_byte,  ("MVI", "L,#${0:X2}") },
            { OpCodes.x2f_CMA,         ("CMA", "") },
            { OpCodes.x30_unknown,     ("???", "") },
            { OpCodes.x31_LXI_SP_word, ("LXI", "SP,#${1:X2}{0:X2}") },
            { OpCodes.x32_STA_addr,    ("STA", "${1:X2}{0:X2}") },
            { OpCodes.x33_INX_SP,      ("INX", "SP") },
            { OpCodes.x34_INR_M,       ("INR", "M") },
            { OpCodes.x35_DCR_M,       ("DCR", "M") },
            { OpCodes.x36_MVI_M_byte,  ("MVI", "M,#${0:X2}") },
            { OpCodes.x37_STC,         ("STC", "") },
            { OpCodes.x38_unknown,     ("???", "") },
            { OpCodes.x39_DAD_SP,      ("DAD", "SP") },
            { OpCodes.x3a_LDA_addr,    ("LDA", "${1:X2}{0:X2}") },
            { OpCodes.x3b_DCX_SP,      ("DCX", "SP") },
            { OpCodes.x3c_INR_A,       ("INR", "A") },
            { OpCodes.x3d_DCR_A,       ("DCR", "A") },
            { OpCodes.x3e_MVI_A_byte,  ("MVI", "A,#${0:X2}") },
            { OpCodes.x3f_CMC,         ("CMC", "") },
            { OpCodes.x40_MOV_B_B,     ("MOV", "B,B") },
            { OpCodes.x41_MOV_B_C,     ("MOV", "B,C") },
            { OpCodes.x42_MOV_B_D,     ("MOV", "B,D") },
            { OpCodes.x43_MOV_B_E,     ("MOV", "B,E") },
            { OpCodes.x44_MOV_B_H,     ("MOV", "B,H") },
            { OpCodes.x45_MOV_B_L,     ("MOV", "B,L") },
            { OpCodes.x46_MOV_B_M,     ("MOV", "B,M") },
            { OpCodes.x47_MOV_B_A,     ("MOV", "B,A") },
            { OpCodes.x48_MOV_C_B,     ("MOV", "C,B") },
            { OpCodes.x49_MOV_C_C,     ("MOV", "C,C") },
            { OpCodes.x4a_MOV_C_D,     ("MOV", "C,D") },
            { OpCodes.x4b_MOV_C_E,     ("MOV", "C,E") },
            { OpCodes.x4c_MOV_C_H,     ("MOV", "C,H") },
            { OpCodes.x4d_MOV_C_L,     ("MOV", "C,L") },
            { OpCodes.x4e_MOV_C_M,     ("MOV", "C,M") },
            { OpCodes.x4f_MOV_C_A,     ("MOV", "C,A") },
            { OpCodes.x50_MOV_D_B,     ("MOV", "D,B") },
            { OpCodes.x51_MOV_D_C,     ("MOV", "D,C") },
            { OpCodes.x52_MOV_D_D,     ("MOV", "D,D") },
            { OpCodes.x53_MOV_D_E,     ("MOV", "D,E") },
            { OpCodes.x54_MOV_D_H,     ("MOV", "D,H") },
            { OpCodes.x55_MOV_D_L,     ("MOV", "D,L") },
            { OpCodes.x56_MOV_D_M,     ("MOV", "D,M") },
            { OpCodes.x57_MOV_D_A,     ("MOV", "D,A") },
            { OpCodes.x58_MOV_E_B,     ("MOV", "E,B") },
            { OpCodes.x59_MOV_E_C,     ("MOV", "E,C") },
            { OpCodes.x5a_MOV_E_D,     ("MOV", "E,D") },
            { OpCodes.x5b_MOV_E_E,     ("MOV", "E,E") },
            { OpCodes.x5c_MOV_E_H,     ("MOV", "E,H") },
            { OpCodes.x5d_MOV_E_L,     ("MOV", "E,L") },
            { OpCodes.x5e_MOV_E_M,     ("MOV", "E,M") },
            { OpCodes.x5f_MOV_E_A,     ("MOV", "E,A") },
            { OpCodes.x60_MOV_H_B,     ("MOV", "H,B") },
            { OpCodes.x61_MOV_H_C,     ("MOV", "H,C") },
            { OpCodes.x62_MOV_H_D,     ("MOV", "H,D") },
            { OpCodes.x63_MOV_H_E,     ("MOV", "H,E") },
            { OpCodes.x64_MOV_H_H,     ("MOV", "H,H") },
            { OpCodes.x65_MOV_H_L,     ("MOV", "H,L") },
            { OpCodes.x66_MOV_H_M,     ("MOV", "H,M") },
            { OpCodes.x67_MOV_H_A,     ("MOV", "H,A") },
            { OpCodes.x68_MOV_L_B,     ("MOV", "L,B") },
            { OpCodes.x69_MOV_L_C,     ("MOV", "L,C") },
            { OpCodes.x6a_MOV_L_D,     ("MOV", "L,D") },
            { OpCodes.x6b_MOV_L_E,     ("MOV", "L,E") },
            { OpCodes.x6c_MOV_L_H,     ("MOV", "L,H") },
            { OpCodes.x6d_MOV_L_L,     ("MOV", "L,L") },
            { OpCodes.x6e_MOV_L_M,     ("MOV", "L,M") },
            { OpCodes.x6f_MOV_L_A,     ("MOV", "L,A") },
            { OpCodes.x70_MOV_M_B,     ("MOV", "M,B") },
            { OpCodes.x71_MOV_M_C,     ("MOV", "M,C") },
            { OpCodes.x72_MOV_M_D,     ("MOV", "M,D") },
            { OpCodes.x73_MOV_M_E,     ("MOV", "M,E") },
            { OpCodes.x74_MOV_M_H,     ("MOV", "M,H") },
            { OpCodes.x75_MOV_M_L,     ("MOV", "M,L") },
            { OpCodes.x76_HLT,         ("HLT", "") },
            { OpCodes.x77_MOV_M_A,     ("MOV", "M,A") },
            { OpCodes.x78_MOV_A_B,     ("MOV", "A,B") },
            { OpCodes.x79_MOV_A_C,     ("MOV", "A,C") },
            { OpCodes.x7a_MOV_A_D,     ("MOV", "A,D") },
            { OpCodes.x7b_MOV_A_E,     ("MOV", "A,E") },
            { OpCodes.x7c_MOV_A_H,     ("MOV", "A,H") },
            { OpCodes.x7d_MOV_A_L,     ("MOV", "A,L") },
            { OpCodes.x7e_MOV_A_M,     ("MOV", "A,M") },
            { OpCodes.x7f_MOV_A_A,     ("MOV", "A,A") },
            { OpCodes.x80_ADD_B,       ("ADD", "B") },
            { OpCodes.x81_ADD_C,       ("ADD", "C") },
            { OpCodes.x82_ADD_D,       ("ADD", "D") },
            { OpCodes.x83_ADD_E,       ("ADD", "E") },
            { OpCodes.x84_ADD_H,       ("ADD", "H") },
            { OpCodes.x85_ADD_L,       ("ADD", "L") },
            { OpCodes.x86_ADD_M,       ("ADD", "M") },
            { OpCodes.x87_ADD_A,       ("ADD", "A") },
            { OpCodes.x88_ADC_B,       ("ADC", "B") },
            { OpCodes.x89_ADC_C,       ("ADC", "C") },
            { OpCodes.x8a_ADC_D,       ("ADC", "D") },
            { OpCodes.x8b_ADC_E,       ("ADC", "E") },
            { OpCodes.x8c_ADC_H,       ("ADC", "H") },
            { OpCodes.x8d_ADC_L,       ("ADC", "L") },
            { OpCodes.x8e_ADC_M,       ("ADC", "M") },
            { OpCodes.x8f_ADC_A,       ("ADC", "A") },
            { OpCodes.x90_SUB_B,       ("SUB", "B") },
            { OpCodes.x91_SUB_C,       ("SUB", "C") },
            { OpCodes.x92_SUB_D,       ("SUB", "D") },
            { OpCodes.x93_SUB_E,       ("SUB", "E") },
            { OpCodes.x94_SUB_H,       ("SUB", "H") },
            { OpCodes.x95_SUB_L,       ("SUB", "L") },
            { OpCodes.x96_SUB_M,       ("SUB", "M") },
            { OpCodes.x97_SUB_A,       ("SUB", "A") },
            { OpCodes.x98_SBB_B,       ("SBB", "B") },
            { OpCodes.x99_SBB_C,       ("SBB", "C") },
            { OpCodes.x9a_SBB_D,       ("SBB", "D") },
            { OpCodes.x9b_SBB_E,       ("SBB", "E") },
            { OpCodes.x9c_SBB_H,       ("SBB", "H") },
            { OpCodes.x9d_SBB_L,       ("SBB", "L") },
            { OpCodes.x9e_SBB_M,       ("SBB", "M") },
            { OpCodes.x9f_SBB_A,       ("SBB", "A") },
            { OpCodes.xa0_ANA_B,       ("ANA", "B") },
            { OpCodes.xa1_ANA_C,       ("ANA", "C") },
            { OpCodes.xa2_ANA_D,       ("ANA", "D") },
            { OpCodes.xa3_ANA_E,       ("ANA", "E") },
            { OpCodes.xa4_ANA_H,       ("ANA", "H") },
            { OpCodes.xa5_ANA_L,       ("ANA", "L") },
            { OpCodes.xa6_ANA_M,       ("ANA", "M") },
            { OpCodes.xa7_ANA_A,       ("ANA", "A") },
            { OpCodes.xa8_ANA_B,       ("XRA", "B") },
            { OpCodes.xa9_XRA_C,       ("XRA", "C") },
            { OpCodes.xaa_XRA_D,       ("XRA", "D") },
            { OpCodes.xab_XRA_E,       ("XRA", "E") },
            { OpCodes.xac_XRA_H,       ("XRA", "H") },
            { OpCodes.xad_XRA_L,       ("XRA", "L") },
            { OpCodes.xae_XRA_M,       ("XRA", "M") },
            { OpCodes.xaf_XRA_A,       ("XRA", "A") },
            { OpCodes.xb0_ORA_B,       ("ORA", "B") },
            { OpCodes.xb1_ORA_C,       ("ORA", "C") },
            { OpCodes.xb2_ORA_D,       ("ORA", "D") },
            { OpCodes.xb3_ORA_E,       ("ORA", "E") },
            { OpCodes.xb4_ORA_H,       ("ORA", "H") },
            { OpCodes.xb5_ORA_L,       ("ORA", "L") },
            { OpCodes.xb6_ORA_M,       ("ORA", "M") },
            { OpCodes.xb7_ORA_A,       ("ORA", "A") },
            { OpCodes.xb8_ORA_B,       ("CMP", "B") },
            { OpCodes.xb9_CMP_C,       ("CMP", "C") },
            { OpCodes.xba_CMP_D,       ("CMP", "D") },
            { OpCodes.xbb_CMP_E,       ("CMP", "E") },
            { OpCodes.xbc_CMP_H,       ("CMP", "H") },
            { OpCodes.xbd_CMP_L,       ("CMP", "L") },
            { OpCodes.xbe_CMP_M,       ("CMP", "M") },
            { OpCodes.xbf_CMP_A,       ("CMP", "A") },
            { OpCodes.xc0_RNZ,         ("RNZ", "") },
            { OpCodes.xc1_POP_B,       ("POP", "B") },
            { OpCodes.xc2_JNZ_addr,    ("JNZ", "${1:X2}{0:X2}") },
            { OpCodes.xc3_JMP_addr,    ("JMP", "${1:X2}{0:X2}") },
            { OpCodes.xc4_CNZ_addr,    ("CNZ", "${1:X2}{0:X2}") },
            { OpCodes.xc5_PUSH_B,      ("PUSH", "B") },
            { OpCodes.xc6_ADI_byte,    ("ADI", "#${0:X2}") },
            { OpCodes.xc7_RST_0,       ("RST", "0") },
            { OpCodes.xc8_RZ,          ("RZ", "") },
            { OpCodes.xc9_RET,         ("RET", "") },
            { OpCodes.xca_JZ_addr,     ("JZ", "${1:X2}{0:X2}") },
            { OpCodes.xcb_unknown,     ("???", "") },
            { OpCodes.xcc_CZ_addr,     ("CZ", "${1:X2}{0:X2}") },
            { OpCodes.xcd_CALL_addr,   ("CALL", "${1:X2}{0:X2}") },
            { OpCodes.xce_ACI_byte,    ("ACI", "#${0:X2}") },
            { OpCodes.xcf_RST_1,       ("RST", "1") },
            { OpCodes.xd0_RNC,         ("RNC", "") },
            { OpCodes.xd1_POP_D,       ("POP", "D") },
            { OpCodes.xd2_JNC_addr,    ("JNC", "${1:X2}{0:X2}") },
            { OpCodes.xd3_OUT_byte,    ("OUT", "#${0:X2}") },
            { OpCodes.xd4_CNC_addr,    ("CNC", "${1:X2}{0:X2}") },
            { OpCodes.xd5_PUSH_D,      ("PUSH", "D") },
            { OpCodes.xd6_SUI_byte,    ("SUI", "#${0:X2}") },
            { OpCodes.xd7_RST_2,       ("RST", "2") },
            { OpCodes.xd8_RC,          ("RC", "") },
            { OpCodes.xd9_unknown,     ("???", "") },
            { OpCodes.xda_JC_addr,     ("JC", "${1:X2}{0:X2}") },
            { OpCodes.xdb_IN_byte,     ("IN", "#${0:X2}") },
            { OpCodes.xdc_CC_addr,     ("CC", "${1:X2}{0:X2}") },
            { OpCodes.xdd_unknown,     ("???", "") },
            { OpCodes.xde_SBI_byte,    ("SBI", "#${0:X2}") },
            { OpCodes.xdf_RST_3,       ("RST", "3") },
            { OpCodes.xe0_RPO,         ("RPO", "") },
            { OpCodes.xe1_POP_H,       ("POP", "H") },
            { OpCodes.xe2_JPO_addr,    ("JPO", "${1:X2}{0:X2}") },
            { OpCodes.xe3_XTHL,        ("XTHL", "") },
            { OpCodes.xe4_CPO_addr,    ("CPO", "${1:X2}{0:X2}") },
            { OpCodes.xe5_PUSH_H,      ("PUSH", "H") },
            { OpCodes.xe6_ANI_byte,    ("ANI", "#${0:X2}") },
            { OpCodes.xe7_RST_4,       ("RST", "4") },
            { OpCodes.xe8_RPE,         ("RPE", "") },
            { OpCodes.xe9_PCHL,        ("PCHL", "") },
            { OpCodes.xea_JPE_addr,    ("JPE", "${1:X2}{0:X2}") },
            { OpCodes.xeb_XCHG,        ("XCHG", "") },
            { OpCodes.xec_CPE_addr,    ("CPE", "${1:X2}{0:X2}") },
            { OpCodes.xed_unknown,     ("???", "") },
            { OpCodes.xee_XRI_byte,    ("XRI", "#${0:X2}") },
            { OpCodes.xef_RST_5,       ("RST", "5") },
            { OpCodes.xf0_RP,          ("RP", "") },
            { OpCodes.xf1_POP_PSW,     ("POP", "PSW") },
            { OpCodes.xf2_JP_addr,     ("JP", "${1:X2}{0:X2}") },
            { OpCodes.xf3_DI,          ("DI", "") },
            { OpCodes.xf4_CP_addr,     ("CP", "${1:X2}{0:X2}") },
            { OpCodes.xf5_PUSH_PSW,    ("PUSH", "PSW") },
            { OpCodes.xf6_ORI_byte,    ("ORI", "#${0:X2}") },
            { OpCodes.xf7_RST_6,       ("RST", "6") },
            { OpCodes.xf8_RM,          ("RM", "") },
            { OpCodes.xf9_SPHL,        ("SPHL", "") },
            { OpCodes.xfa_JM_addr,     ("JM", "${1:X2}{0:X2}") },
            { OpCodes.xfb_EI,          ("EI", "") },
            { OpCodes.xfc_CM_addr,     ("CM", "${1:X2}{0:X2}") },
            { OpCodes.xfd_unknown,     ("???", "") },
            { OpCodes.xfe_CPI_byte,    ("CPI", "#${0:X2}") },
            { OpCodes.xff_RST_7,       ("RST", "7") },
        };

        private string FormatDisassembledLine(IMemoryManager memory, UInt16 pc, string mnemonic, string parameterFormat = "")
        {
            var instructionLine = new StringBuilder();
            var parameterCount = GetParameterCountFromParameterFormat(parameterFormat);
            instructionLine.Append($"{pc:X4}  ");
            instructionLine.Append(FormatInstructionBytesAsHex(memory, pc, 1 + parameterCount));
            string parametersString = GetInstructionParametersAsString(memory, pc, parameterFormat, parameterCount);

            instructionLine.Append($" {mnemonic,-6} {parametersString,-11}");
            return instructionLine.ToString();
        }

        private byte GetParameterCountFromParameterFormat(string parameterFormat)
        {
            var parametersPattern = new Regex(@"\{\d[^\}]*\}");
            return (byte)parametersPattern.Matches(parameterFormat).Count;
        }

        private string FormatInstructionBytesAsHex(IMemoryManager memory, UInt16 pc, int numberOfBytes)
        {
            StringBuilder instructionHex = new StringBuilder("");
            for (int index = numberOfBytes - 1; index >= 0; index--)
            {
                instructionHex.Insert(0, string.Format("{0:X2} ", memory.Read_8(pc + index)));
            }
            while (instructionHex.Length / 3 < 3)
            {
                instructionHex.Append("   ");
            }
            return instructionHex.ToString();
        }

        private string GetInstructionParametersAsString(IMemoryManager memory, UInt16 pc, string parameterFormat, int parameterCount)
        {
            var parameterBytes = new object[parameterCount];
            for(var address = pc +1; address <= pc + parameterCount; address++)
            {
                parameterBytes[address - pc - 1] = memory.Read_8(address);
            }
            return string.Format(parameterFormat, parameterBytes);

        }
    }
}
