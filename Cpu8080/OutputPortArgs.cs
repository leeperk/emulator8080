﻿namespace Cpu8080
{
    public class OutputPortArgs
    {
        public byte Port { get; }
        public byte Data { get; }
        
        public OutputPortArgs(byte port, byte data)
        {
            Port = port;
            Data = data;
        }
    }
}
