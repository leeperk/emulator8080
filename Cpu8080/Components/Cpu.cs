﻿using Memory;
using Ports;
using System;
using System.Collections;
using System.Linq;

namespace Cpu8080.Components
{
    public class Cpu
    {
        private Action[] _opCodeFunctions;
        private byte[] _instructionCycles;
        private Disassembler _disassembler = new Disassembler();
        private IMemoryManager _memoryManager;
        private IPortManager _portManager;
        private byte[] _szpFlags = new byte[256];

        public delegate byte GetDataFromPortFunction(byte port);
        public delegate void SendDataToPortFunction(byte port, byte data);

        public CpuState State { get; }
        public long ExecutedInstructionCount { get; set; }

        public Cpu(IMemoryManager memory, IPortManager portManager)
        {
            _memoryManager = memory;
            _portManager = portManager;
            _opCodeFunctions = GenerateOpCodeFunctionList();
            _instructionCycles = GenerateInstructionCycles();
            InitializePrecalculatedFlags();
            State = new CpuState();
            ExecutedInstructionCount = 0;
        }

        public byte ExecuteNextInstruction()
        {
            byte opCode = fetchOpCode();
            var opCodeFunction = _opCodeFunctions[opCode];

            opCodeFunction();
            ExecutedInstructionCount++;
            return _instructionCycles[opCode];
        }

        public void Interrupt(byte number, bool maskable = true)
        {
            if (false == maskable || State.InterruptsEnabled == 1)
            {
                State.InterruptsEnabled = 0;
                Execute_PUSH(State.PC);
                State.PC = (UInt16)(8 * number);
            }
        }

        private Action[] GenerateOpCodeFunctionList()
        {
            return new Action[]
            {
                /* 00 */ Execute_NOP,
                /* 01 */ () => State.BC = Execute_LXI(),
                /* 02 */ () => Execute_STAX(State.BC),
                /* 03 */ () => State.BC = Execute_INX(State.BC),
                /* 04 */ () => State.B = Execute_INR(State.B),
                /* 05 */ () => State.B = Execute_DCR(State.B),
                /* 06 */ () => State.B = Execute_MVI(),
                /* 07 */ Execute_RLC,
                /* 08 */ UnimplementedInstruction,
                /* 09 */ () => Execute_DAD(State.BC),
                /* 0A */ () => Execute_LDAX(State.BC),
                /* 0B */ () => State.BC = Execute_DCX(State.BC),
                /* 0C */ () => State.C = Execute_INR(State.C),
                /* 0D */ () => State.C = Execute_DCR(State.C),
                /* 0E */ () => State.C = Execute_MVI(),
                /* 0F */ Execute_RRC,
                /* 10 */ UnimplementedInstruction,
                /* 11 */ () => State.DE = Execute_LXI(),
                /* 12 */ () => Execute_STAX(State.DE),
                /* 13 */ () => State.DE = Execute_INX(State.DE),
                /* 14 */ () => State.D = Execute_INR(State.D),
                /* 15 */ () => State.D = Execute_DCR(State.D),
                /* 16 */ () => State.D = Execute_MVI(),
                /* 17 */ Execute_RAL,
                /* 18 */ UnimplementedInstruction,
                /* 19 */ () => Execute_DAD(State.DE),
                /* 1A */ () => Execute_LDAX(State.DE),
                /* 1B */ () => State.DE = Execute_DCX(State.DE),
                /* 1C */ () => State.E = Execute_INR(State.E),
                /* 1D */ () => State.E = Execute_DCR(State.E),
                /* 1E */ () => State.E = Execute_MVI(),
                /* 1F */ Execute_RAR,
                /* 20 */ UnimplementedInstruction,
                /* 21 */ () => State.HL = Execute_LXI(),
                /* 22 */ Execute_SHLD,
                /* 23 */ () => State.HL = Execute_INX(State.HL),
                /* 24 */ () => State.H = Execute_INR(State.H),
                /* 25 */ () => State.H = Execute_DCR(State.H),
                /* 26 */ () => State.H = Execute_MVI(),
                /* 27 */ Execute_DAA,
                /* 28 */ UnimplementedInstruction,
                /* 29 */ () => Execute_DAD(State.HL),
                /* 2A */ Execute_LHLD,
                /* 2B */ () => State.HL = Execute_DCX(State.HL),
                /* 2C */ () => State.L = Execute_INR(State.L),
                /* 2D */ () => State.L = Execute_DCR(State.L),
                /* 2E */ () => State.L = Execute_MVI(),
                /* 2F */ Execute_CMA,
                /* 30 */ UnimplementedInstruction,
                /* 31 */ () => State.SP = Execute_LXI(),
                /* 32 */ Execute_STA,
                /* 33 */ () => State.SP = Execute_INX(State.SP),
                /* 34 */ () => _memoryManager.Write_8(State.HL, Execute_INR(_memoryManager.Read_8(State.HL))),
                /* 35 */ () => _memoryManager.Write_8(State.HL, Execute_DCR(_memoryManager.Read_8(State.HL))),
                /* 36 */ () => _memoryManager.Write_8(State.HL, Execute_MVI()),
                /* 37 */ Execute_STC,
                /* 38 */ UnimplementedInstruction,
                /* 39 */ () => Execute_DAD(State.SP),
                /* 3A */ Execute_LDA,
                /* 3B */ () => State.SP = Execute_DCX(State.SP),
                /* 3C */ () => State.A = Execute_INR(State.A),
                /* 3D */ () => State.A = Execute_DCR(State.A),
                /* 3E */ () => State.A = Execute_MVI(),
                /* 3F */ Execute_CMC,
                /* 40 */ () => State.B = State.B,
                /* 41 */ () => State.B = State.C,
                /* 42 */ () => State.B = State.D,
                /* 43 */ () => State.B = State.E,
                /* 44 */ () => State.B = State.H,
                /* 45 */ () => State.B = State.L,
                /* 46 */ () => State.B = _memoryManager.Read_8(State.HL),
                /* 47 */ () => State.B = State.A,
                /* 48 */ () => State.C = State.B,
                /* 49 */ () => State.C = State.C,
                /* 4A */ () => State.C = State.D,
                /* 4B */ () => State.C = State.E,
                /* 4C */ () => State.C = State.H,
                /* 4D */ () => State.C = State.L,
                /* 4E */ () => State.C = _memoryManager.Read_8(State.HL),
                /* 4F */ () => State.C = State.A,
                /* 50 */ () => State.D = State.B,
                /* 51 */ () => State.D = State.C,
                /* 52 */ () => State.D = State.D,
                /* 53 */ () => State.D = State.E,
                /* 54 */ () => State.D = State.H,
                /* 55 */ () => State.D = State.L,
                /* 56 */ () => State.D = _memoryManager.Read_8(State.HL),
                /* 57 */ () => State.D = State.A,
                /* 58 */ () => State.E = State.B,
                /* 59 */ () => State.E = State.C,
                /* 5A */ () => State.E = State.D,
                /* 5B */ () => State.E = State.E,
                /* 5C */ () => State.E = State.H,
                /* 5D */ () => State.E = State.L,
                /* 5E */ () => State.E = _memoryManager.Read_8(State.HL),
                /* 5F */ () => State.E = State.A,
                /* 60 */ () => State.H = State.B,
                /* 61 */ () => State.H = State.C,
                /* 62 */ () => State.H = State.D,
                /* 63 */ () => State.H = State.E,
                /* 64 */ () => State.H = State.H,
                /* 65 */ () => State.H = State.L,
                /* 66 */ () => State.H = _memoryManager.Read_8(State.HL),
                /* 67 */ () => State.H = State.A,
                /* 68 */ () => State.L = State.B,
                /* 69 */ () => State.L = State.C,
                /* 6A */ () => State.L = State.D,
                /* 6B */ () => State.L = State.E,
                /* 6C */ () => State.L = State.H,
                /* 6D */ () => State.L = State.L,
                /* 6E */ () => State.L = _memoryManager.Read_8(State.HL),
                /* 6F */ () => State.L = State.A,
                /* 70 */ () => _memoryManager.Write_8(State.HL, State.B),
                /* 71 */ () => _memoryManager.Write_8(State.HL, State.C),
                /* 72 */ () => _memoryManager.Write_8(State.HL, State.D),
                /* 73 */ () => _memoryManager.Write_8(State.HL, State.E),
                /* 74 */ () => _memoryManager.Write_8(State.HL, State.H),
                /* 75 */ () => _memoryManager.Write_8(State.HL, State.L),
                /* 76 */ Execute_HLT,
                /* 77 */ () => _memoryManager.Write_8(State.HL, State.A),
                /* 78 */ () => State.A = State.B,
                /* 79 */ () => State.A = State.C,
                /* 7A */ () => State.A = State.D,
                /* 7B */ () => State.A = State.E,
                /* 7C */ () => State.A = State.H,
                /* 7D */ () => State.A = State.L,
                /* 7E */ () => State.A = _memoryManager.Read_8(State.HL),
                /* 7F */ () => State.A = State.A,
                /* 80 */ () => Execute_ADD(State.B),
                /* 81 */ () => Execute_ADD(State.C),
                /* 82 */ () => Execute_ADD(State.D),
                /* 83 */ () => Execute_ADD(State.E),
                /* 84 */ () => Execute_ADD(State.H),
                /* 85 */ () => Execute_ADD(State.L),
                /* 86 */ () => Execute_ADD(_memoryManager.Read_8(State.HL)),
                /* 87 */ () => Execute_ADD(State.A),
                /* 88 */ () => Execute_ADC(State.B),
                /* 89 */ () => Execute_ADC(State.C),
                /* 8A */ () => Execute_ADC(State.D),
                /* 8B */ () => Execute_ADC(State.E),
                /* 8C */ () => Execute_ADC(State.H),
                /* 8D */ () => Execute_ADC(State.L),
                /* 8E */ () => Execute_ADC(_memoryManager.Read_8(State.HL)),
                /* 8F */ () => Execute_ADC(State.A),
                /* 90 */ () => Execute_SUB(State.B),
                /* 91 */ () => Execute_SUB(State.C),
                /* 92 */ () => Execute_SUB(State.D),
                /* 93 */ () => Execute_SUB(State.E),
                /* 94 */ () => Execute_SUB(State.H),
                /* 95 */ () => Execute_SUB(State.L),
                /* 96 */ () => Execute_SUB(_memoryManager.Read_8(State.HL)),
                /* 97 */ () => Execute_SUB(State.A),
                /* 98 */ () => Execute_SBB(State.B),
                /* 99 */ () => Execute_SBB(State.C),
                /* 9A */ () => Execute_SBB(State.D),
                /* 9B */ () => Execute_SBB(State.E),
                /* 9C */ () => Execute_SBB(State.H),
                /* 9D */ () => Execute_SBB(State.L),
                /* 9E */ () => Execute_SBB(_memoryManager.Read_8(State.HL)),
                /* 9F */ () => Execute_SBB(State.A),
                /* A0 */ () => Execute_ANA(State.B),
                /* A1 */ () => Execute_ANA(State.C),
                /* A2 */ () => Execute_ANA(State.D),
                /* A3 */ () => Execute_ANA(State.E),
                /* A4 */ () => Execute_ANA(State.H),
                /* A5 */ () => Execute_ANA(State.L),
                /* A6 */ () => Execute_ANA(_memoryManager.Read_8(State.HL)),
                /* A7 */ () => Execute_ANA(State.A),
                /* A8 */ () => Execute_XRA(State.B),
                /* A9 */ () => Execute_XRA(State.C),
                /* AA */ () => Execute_XRA(State.D),
                /* AB */ () => Execute_XRA(State.E),
                /* AC */ () => Execute_XRA(State.H),
                /* AD */ () => Execute_XRA(State.L),
                /* AE */ () => Execute_XRA(_memoryManager.Read_8(State.HL)),
                /* AF */ () => Execute_XRA(State.A),
                /* B0 */ () => Execute_ORA(State.B),
                /* B1 */ () => Execute_ORA(State.C),
                /* B2 */ () => Execute_ORA(State.D),
                /* B3 */ () => Execute_ORA(State.E),
                /* B4 */ () => Execute_ORA(State.H),
                /* B5 */ () => Execute_ORA(State.L),
                /* B6 */ () => Execute_ORA(_memoryManager.Read_8(State.HL)),
                /* B7 */ () => Execute_ORA(State.A),
                /* B8 */ () => Execute_CMP(State.B),
                /* B9 */ () => Execute_CMP(State.C),
                /* BA */ () => Execute_CMP(State.D),
                /* BB */ () => Execute_CMP(State.E),
                /* BC */ () => Execute_CMP(State.H),
                /* BD */ () => Execute_CMP(State.L),
                /* BE */ () => Execute_CMP(_memoryManager.Read_8(State.HL)),
                /* BF */ () => Execute_CMP(State.A),
                /* C0 */ Execute_RNZ,
                /* C1 */ () => State.BC = Execute_POP(),
                /* C2 */ Execute_JNZ,
                /* C3 */ () => Execute_JMP(true),
                /* C4 */ Execute_CNZ,
                /* C5 */ () => Execute_PUSH(State.BC),
                /* C6 */ Execute_ADI,
                /* C7 */ () => Execute_RST(0),
                /* C8 */ Execute_RZ,
                /* C9 */ () => Execute_RET(true),
                /* CA */ Execute_JZ,
                /* CB */ () => Execute_JMP(true), // Undocument JMP <address>
                /* CC */ Execute_CZ,
                /* CD */ () => Execute_CALL(true),
                /* CE */ Execute_ACI,
                /* CF */ () => Execute_RST(1),
                /* D0 */ Execute_RNC,
                /* D1 */ () => State.DE = Execute_POP(),
                /* D2 */ Execute_JNC,
                /* D3 */ Execute_OUT,
                /* D4 */ Execute_CNC,
                /* D5 */ () => Execute_PUSH(State.DE),
                /* D6 */ Execute_SUI,
                /* D7 */ () => Execute_RST(2),
                /* D8 */ Execute_RC,
                /* D9 */ () => Execute_RET(true), // Undocumented Return
                /* DA */ Execute_JC,
                /* DB */ Execute_IN,
                /* DC */ Execute_CC,
                /* DD */ () => Execute_CALL(true), // Undocumented CALL <address>
                /* DF */ Execute_SBI,
                /* DF */ () => Execute_RST(3),
                /* E0 */ Execute_RPO,
                /* E1 */ () => State.HL = Execute_POP(),
                /* E2 */ Execute_JPO,
                /* E3 */ Execute_XTHL,
                /* E4 */ Execute_CPO,
                /* E5 */ () => Execute_PUSH(State.HL),
                /* E6 */ Execute_ANI,
                /* E7 */ () => Execute_RST(4),
                /* E8 */ Execute_RPE,
                /* E9 */ Execute_PCHL,
                /* EA */ Execute_JPE,
                /* EB */ Execute_XCHG,
                /* EC */ Execute_CPE,
                /* ED */ () => Execute_CALL(true), // Undocumented CALL <address>
                /* EE */ Execute_XRI,
                /* EF */ () => Execute_RST(5),
                /* F0 */ Execute_RP,
                /* F1 */ () => State.AF = Execute_POP(),
                /* F2 */ Execute_JP,
                /* F3 */ Execute_DI,
                /* F4 */ Execute_CP,
                /* F5 */ () => Execute_PUSH(State.AF),
                /* F6 */ Execute_ORI,
                /* F7 */ () => Execute_RST(6),
                /* F8 */ Execute_RM,
                /* F9 */ Execute_SPHL,
                /* FA */ Execute_JM,
                /* FB */ Execute_EI,
                /* FC */ Execute_CM,
                /* FD */ () => Execute_CALL(true), // Undocumented CALL <address>
                /* FE */ Execute_CPI,
                /* FF */ () => Execute_RST(7)
            };
        }

        private byte[] GenerateInstructionCycles()
        {
            return new byte[] {
                /* 00-0F */  4, 10,  7,  5,  5,  5,  7,  4,  4, 10,  7,  5,  5,  5,  7,  4,
                /* 10-1F */  4, 10,  7,  5,  5,  5,  7,  4,  4, 10,  7,  5,  5,  5,  7,  4,
                /* 20-2F */  4, 10, 16,  5,  5,  5,  7,  4,  4, 10, 16,  5,  5,  5,  7,  4,
                /* 30-3F */  4, 10, 13,  5, 10, 10, 10,  4,  4, 10, 13,  5,  5,  5,  7,  4,

                /* 40-4F */  5,  5,  5,  5,  5,  5,  7,  5,  5,  5,  5,  5,  5,  5,  7,  5,
                /* 50-5F */  5,  5,  5,  5,  5,  5,  7,  5,  5,  5,  5,  5,  5,  5,  7,  5,
                /* 60-6F */  5,  5,  5,  5,  5,  5,  7,  5,  5,  5,  5,  5,  5,  5,  7,  5,
                /* 70-7F */  7,  7,  7,  7,  7,  7,  7,  7,  5,  5,  5,  5,  5,  5,  7,  5,

                /* 80-8F */  4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
                /* 90-9F */  4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
                /* A0-AF */  4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
                /* B0-BF */  4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,

                /* C0-CF */ 11, 10, 10, 10, 17, 11,  7, 11, 11, 10, 10, 10, 10, 17,  7, 11,
                /* D0-DF */ 11, 10, 10, 10, 17, 11,  7, 11, 11, 10, 10, 10, 10, 17,  7, 11,
                /* E0-EF */ 11, 10, 10, 18, 17, 11,  7, 11, 11,  5, 10,  5, 17, 17,  7, 11,
                /* F0-FF */ 11, 10, 10,  4, 17, 11,  7, 11, 11,  5, 10,  4, 17, 17,  7, 11,
            };
        }

        private void UnimplementedInstruction()
        {
            //Console.WriteLine("Error: Unimplemented instruction");
            //_disassembler.GenerateDisassembledInstructionForDisplay(_memory, State.pc);
            //_disassembler.GenerateStateForDisplay(State, _memory);
            //throw new InvalidOperationException();
        }

        private bool IsByteEvenParity(byte value)
        {
            var bits = new BitArray(new[] { value });
            var totalBits = bits.Cast<bool>().Count(b => b);
            return (totalBits & 1) == 0;
        }

        private bool IsWordEvenParity(UInt16 value)
        {
            byte highByte = (byte)((value & 0xff00) >> 8);
            byte lowByte = (byte)(value & 0xff);
            return IsByteEvenParity(highByte) && IsByteEvenParity(lowByte);
        }

        private void InitializePrecalculatedFlags()
        {
            for(var index = 0; index <= 0xFF; index++)
            {
                byte zs = 0;
                byte parity = 0;

                if (index == 0) { zs |= CpuState.ZeroFlagMask; }
                if((index & 0x80) == 0x80) { zs |= CpuState.SignFlagMask; }
                if ((index & 0x01) == 0x01) { parity++; }
                if ((index & 0x02) == 0x02) { parity++; }
                if ((index & 0x04) == 0x04) { parity++; }
                if ((index & 0x08) == 0x08) { parity++; }
                if ((index & 0x10) == 0x10) { parity++; }
                if ((index & 0x20) == 0x20) { parity++; }
                if ((index & 0x40) == 0x40) { parity++; }
                if ((index & 0x80) == 0x80) { parity++; }

                _szpFlags[index] = (byte)(zs | ((parity & 0x01) == 0x01 ? 0 : CpuState.ParityFlagMask));
            }
        }

        private byte fetchOpCode()
        {
            return _memoryManager.Read_8(State.PC++);
        }

        private byte fetchArgument_8()
        {
            return _memoryManager.Read_8(State.PC++);
        }

        private UInt16 fetchArgument_16()
        {
            return (UInt16)(_memoryManager.Read_8(State.PC++) | (_memoryManager.Read_8(State.PC++) << 8));
        }

        #region Op Code Functions

        private void Execute_NOP()
        {
        }

        private UInt16 Execute_LXI()
        {
            return fetchArgument_16();
        }

        private void Execute_STAX(UInt16 address)
        {
            _memoryManager.Write_8(address, State.A);
        }

        private UInt16 Execute_INX(UInt16 value)
        {
            UInt32 answer = (UInt32)value + 1;

            return ((UInt16)(answer & 0xFFFF));
        }

        private byte Execute_INR(byte value)
        {
            byte hFlag = (byte)((value & 0x0F) == 0x0F ? CpuState.HalfCarryFlagMask : 0);
            State.Flags = (byte)((State.Flags & CpuState.CarryFlagMask) | _szpFlags[++value] | hFlag);

            return value;
        }

        private byte Execute_DCR(byte value)
        {
            byte hFlag = (byte)((value & 0x0F) != 0x00 ? CpuState.HalfCarryFlagMask : 0);
            State.Flags = (byte)((State.Flags & CpuState.CarryFlagMask) | _szpFlags[--value] | hFlag);

            return value;
        }

        private byte Execute_MVI()
        {
            return fetchArgument_8();
        }

        private void Execute_RLC()
        {
            State.A = (byte)((State.A << 1) | (State.A >> 7));
            State.Flags = (byte)((State.Flags & 0xFE) | (State.A & CpuState.CarryFlagMask));
        }

        private void Execute_DAD(UInt16 value)
        {
            UInt32 answer = (UInt32)(((State.H << 8) | State.L) + value);
            State.Flags = (byte)((State.Flags & ~CpuState.CarryFlagMask) | ((byte)(answer >> 16) & CpuState.CarryFlagMask));
            State.H = (byte)((answer >> 8) & 0xFF);
            State.L = (byte)(answer & 0xFF);
        }

        private void Execute_LDAX(UInt16 address)
        {
            State.A = _memoryManager.Read_8(address);
        }

        private UInt16 Execute_DCX(UInt16 value)
        {
            UInt32 answer = (UInt32)(value - 1);
            return (UInt16)(answer & 0xFFFF);
        }

        private void Execute_RRC()
        {
            State.Flags = (byte)((State.Flags & 0xFE) | (State.A & CpuState.CarryFlagMask));
            State.A = (byte)((State.A >> 1) | (State.A << 7));
        }

        private void Execute_RAL()
        {
            byte originalCarry = (byte)(State.Flags & CpuState.CarryFlagMask);
            State.Flags = (byte)((State.Flags & 0xFE) | (State.A >> 7));
            State.A = (byte)((State.A << 1) | originalCarry);
        }

        private void Execute_RAR()
        {
            byte originalCarryInBit7 = (byte)((State.Flags & CpuState.CarryFlagMask) << 7);
            State.Flags = (byte)((State.Flags & 0xFE) | (State.A & CpuState.CarryFlagMask));
            State.A = (byte)((State.A >> 1) | originalCarryInBit7);
        }

        private void Execute_SHLD()
        {
            UInt16 address = fetchArgument_16();
            _memoryManager.Write_8(address, State.L);
            _memoryManager.Write_8(address + 1, State.H);
        }

        private void Execute_DAA()
        {
            byte answer = State.A;
            if (((State.Flags & CpuState.HalfCarryFlagMask) > 0) || ((State.A & 0x0F) > 9))
            {
                answer += 0x06;
            }
            if (((State.Flags & CpuState.CarryFlagMask) > 0) || State.A > 0x99)
            {
                answer += 0x60;
            }
            State.Flags = (byte)((State.Flags & 3) | (State.A & 0x28) | ((State.A > 0x99) ? 1 : 0) | ((State.A ^ answer) & 0x10) | _szpFlags[answer]);
            State.A = answer;
        }

        private void Execute_LHLD()
        {
            UInt16 address = fetchArgument_16();
            State.L = _memoryManager.Read_8(address);
            State.H = _memoryManager.Read_8(address + 1);
        }

        private void Execute_CMA()
        {
            State.A ^= 0xFF;
        }

        private void Execute_STA()
        {
            UInt16 address = fetchArgument_16();
            _memoryManager.Write_8(address, State.A);
        }

        private void Execute_STC()
        {
            State.Flags = (byte)((State.Flags & 0xFE) | CpuState.CarryFlagMask);
        }

        private void Execute_LDA()
        {
            UInt16 address = fetchArgument_16();
            State.A = _memoryManager.Read_8(address);
        }

        private void Execute_CMC()
        {
            State.Flags = (byte)((State.Flags & 0xFE) | (~State.Flags & CpuState.CarryFlagMask));
        }

        private void Execute_HLT()
        {
            UnimplementedInstruction();
        }

        private void  Execute_ADD(byte value)
        {
            UInt16 answer = (UInt16)(State.A + value);
            State.Flags = (byte)(_szpFlags[answer & 0xFF] | ((answer >> 8) & CpuState.CarryFlagMask) | ((State.A ^ answer ^ value) & CpuState.HalfCarryFlagMask));
            State.A = (byte)(answer & 0xFF);
        }

        private void Execute_ADC(byte value)
        {
            UInt16 answer = (UInt16)(State.A + value + (State.Flags & CpuState.CarryFlagMask));
            State.Flags = (byte)(_szpFlags[answer & 0xFF] | ((answer >> 8) & CpuState.CarryFlagMask) | ((State.A ^ answer ^ value) & CpuState.HalfCarryFlagMask));
            State.A = (byte)(answer & 0xFF);
        }

        private void Execute_SUB(byte value)
        {
            UInt16 answer = (UInt16)(State.A - value);
            State.Flags = (byte)(_szpFlags[answer & 0xFF] | ((answer >> 8) & CpuState.CarryFlagMask) | (~(State.A ^ answer ^ value) & CpuState.HalfCarryFlagMask));
            State.A = (byte)(answer & 0xFF);
        }

        private void Execute_SBB(byte value)
        {
            UInt16 answer = (UInt16)(State.A - value - (State.Flags & CpuState.CarryFlagMask));
            State.Flags = (byte)(_szpFlags[answer & 0xFF] | ((answer >> 8) & CpuState.CarryFlagMask) | (~(State.A ^ answer ^ value) & CpuState.HalfCarryFlagMask));
            State.A = (byte)(answer & 0xFF);
        }

        private void Execute_ANA(byte value)
        {
            byte hFlag = (byte)(((State.A | value) << 1) & CpuState.HalfCarryFlagMask);
            State.A &= value;
            State.Flags = _szpFlags[State.A];
            State.Flags |= hFlag;
        }

        private void Execute_XRA(byte value)
        {
            State.A ^= value;
            State.Flags = _szpFlags[State.A];
        }

        private void Execute_ORA(byte value)
        {
            State.A |= value;
            State.Flags = _szpFlags[State.A];
        }

        private void Execute_CMP(byte value)
        {
            UInt16 answer = (UInt16)(State.A - value);
            State.Flags = (byte)(_szpFlags[answer & 0xFF] | ((answer >> 8) & CpuState.CarryFlagMask) | (~(State.A ^ answer ^ value) & CpuState.HalfCarryFlagMask));
        }

        private void Execute_RNZ() => Execute_RET((State.Flags & CpuState.ZeroFlagMask) == 0);

        private UInt16 Execute_POP()
        {
            State.SP += 2;
            return _memoryManager.Read_16(State.SP - 2);
        }

        private void Execute_JNZ() => Execute_JMP((State.Flags & CpuState.ZeroFlagMask) == 0);

        private void Execute_JMP(bool shouldJump)
        {
            UInt16 address = fetchArgument_16();
            if (shouldJump)
            {
                State.PC = address;
            }
        }

        private void Execute_CNZ() => Execute_CALL((State.Flags & CpuState.ZeroFlagMask) == 0);

        private void Execute_PUSH(UInt16 word)
        {
            State.SP -= 2;
            _memoryManager.Write_16(State.SP, word);
        }

        private void Execute_ADI() => Execute_ADD(fetchArgument_8());

        private void Execute_RST(byte value) => Interrupt(value, false);

        private void Execute_RZ() => Execute_RET((State.Flags & CpuState.ZeroFlagMask) > 0);

        private void Execute_RET(bool shouldReturn)
        {
            if (shouldReturn)
            {
                State.PC = Execute_POP();
            }
        }

        private void Execute_JZ() => Execute_JMP((State.Flags & CpuState.ZeroFlagMask) > 0);

        private void Execute_CZ() => Execute_CALL((State.Flags & CpuState.ZeroFlagMask) > 0);

        private void Execute_CALL(bool shouldCall)
        {
            UInt16 callAddress = fetchArgument_16();
            if (shouldCall)
            {
                UInt16 returnAddress = State.PC;
                Execute_PUSH(returnAddress);
                State.PC = callAddress;
            }
        }

        private void  Execute_ACI() => Execute_ADC(fetchArgument_8());

        private void Execute_RNC() => Execute_RET((State.Flags & CpuState.CarryFlagMask) == 0);

        private void Execute_JNC() => Execute_JMP((State.Flags & CpuState.CarryFlagMask) == 0);

        private void Execute_OUT()
        {
            byte port = fetchArgument_8();
            byte data = State.A;
            _portManager.WritePort(port, data);
        }

        private void Execute_CNC() => Execute_CALL((State.Flags & CpuState.CarryFlagMask) == 0);

        private void Execute_SUI() => Execute_SUB(fetchArgument_8());

        private void Execute_RC() => Execute_RET((State.Flags & CpuState.CarryFlagMask) > 0);

        private void Execute_JC() => Execute_JMP((State.Flags & CpuState.CarryFlagMask) > 0);

        private void Execute_IN()
        {
            byte port = fetchArgument_8();
            State.A = _portManager.ReadPort(port);
        }

        private void Execute_CC() => Execute_CALL((State.Flags & CpuState.CarryFlagMask) > 0);

        private void Execute_SBI() => Execute_SBB(fetchArgument_8());

        private void Execute_RPO() => Execute_RET((State.Flags & CpuState.ParityFlagMask) == 0);

        private void Execute_JPO() => Execute_JMP((State.Flags & CpuState.ParityFlagMask) == 0);

        private void Execute_XTHL()
        {
            UInt16 temp = Execute_POP();
            Execute_PUSH(State.HL);
            State.H = (byte)((temp >> 8) & 0xFF);
            State.L = (byte)(temp & 0xFF);
        }

        private void Execute_CPO() => Execute_CALL((State.Flags & CpuState.ParityFlagMask) == 0);

        private void Execute_ANI() => Execute_ANA(fetchArgument_8());

        private void Execute_RPE() => Execute_RET((State.Flags & CpuState.ParityFlagMask) > 0);

        private void Execute_PCHL()
        {
            State.PC = (UInt16)((State.H << 8) | State.L);
        }

        private void Execute_JPE() => Execute_JMP((State.Flags & CpuState.ParityFlagMask) > 0);

        private void Execute_XCHG()
        {
            byte temp = State.H;
            State.H = State.D;
            State.D = temp;
            temp = State.L;
            State.L = State.E;
            State.E = temp;
        }

        private void Execute_CPE() => Execute_CALL((State.Flags & CpuState.ParityFlagMask) > 0);

        private void Execute_XRI() => Execute_XRA(fetchArgument_8());

        private void Execute_RP() => Execute_RET((State.Flags & CpuState.SignFlagMask) == 0);

        private void Execute_JP() => Execute_JMP((State.Flags & CpuState.SignFlagMask) == 0);

        private void Execute_DI() => State.InterruptsEnabled = 0;

        private void Execute_CP() => Execute_CALL((State.Flags & CpuState.SignFlagMask) == 0);

        private void Execute_ORI() => Execute_ORA(fetchArgument_8());

        private void Execute_RM() => Execute_RET((State.Flags & CpuState.SignFlagMask) > 0);

        private void Execute_SPHL() => State.SP = (UInt16)((State.H << 8) | State.L);

        private void Execute_JM() => Execute_JMP((State.Flags & CpuState.SignFlagMask) > 0);

        private void Execute_EI() => State.InterruptsEnabled = 1;

        private void Execute_CM() => Execute_CALL((State.Flags & CpuState.SignFlagMask) > 0);

        private void Execute_CPI() => Execute_CMP(fetchArgument_8());

        #endregion

    }
}
