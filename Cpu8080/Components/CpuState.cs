﻿using System;
using System.Linq;

namespace Cpu8080.Components
{
    public class CpuState
    {
        private Register _af { get; set; }
        private Register _bc { get; set; }
        private Register _de { get; set; }
        private Register _hl { get; set; }
        private Register _sp { get; set; }
        private Register _pc { get; set; }

        public UInt16 AF { get { return _af.Word; } set { _af.Word = value; } }
        public byte A { get { return _af.High; } set { _af.High = value; } }
        public byte Flags { get { return _af.Low; } set { _af.Low= value; } }

        public UInt16 BC { get { return _bc.Word; } set { _bc.Word = value; } }
        public byte B { get { return _bc.High; } set { _bc.High = value; } }
        public byte C { get { return _bc.Low; } set { _bc.Low = value; } }

        public UInt16 DE { get { return _de.Word; } set { _de.Word = value; } }
        public byte D { get { return _de.High; } set { _de.High = value; } }
        public byte E { get { return _de.Low; } set { _de.Low = value; } }

        public UInt16 HL { get { return _hl.Word; } set { _hl.Word = value; } }
        public byte H { get { return _hl.High; } set { _hl.High = value; } }
        public byte L { get { return _hl.Low; } set { _hl.Low = value; } }

        public UInt16 SP { get { return _sp.Word; } set { _sp.Word = value; } }

        public UInt16 PC { get { return _pc.Word; } set { _pc.Word = value; } }

        public byte InterruptsEnabled { get; set; }

        public const byte CarryFlagMask = 0x01;
        public const byte ParityFlagMask = 0x04;
        public const byte HalfCarryFlagMask = 0x10;
        public const byte ZeroFlagMask = 0x40;
        public const byte SignFlagMask = 0x80;

        public CpuState()
        {
            _af = new Register();
            _bc = new Register();
            _de = new Register();
            _hl = new Register();
            _sp = new Register();
            _pc = new Register();
            InterruptsEnabled = 0;
        }

        public CpuState(CpuState stateToClone) : base()
        {
            AF = stateToClone.AF;
            BC = stateToClone.BC;
            DE = stateToClone.DE;
            HL = stateToClone.HL;
            SP = stateToClone.SP;
            PC = stateToClone.PC;
            InterruptsEnabled = stateToClone.InterruptsEnabled;
        }
    }
}
