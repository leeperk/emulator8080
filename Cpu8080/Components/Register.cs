﻿using System;

namespace Cpu8080.Components
{
    public class Register
    {
        public byte Low { get; set; }

        public byte High { get; set; }

        public UInt16 Word
        {
            get { return (UInt16)((High << 8) | Low); }
            set
            {
                Low = (byte)(value & 0xFF);
                High = (byte)((value >> 8) & 0xFF);
            }
        }

        public Register()
        {
            Low = 0;
            High = 0;
        }
    }
}
