﻿using System;

namespace Memory
{
    public class MemoryReadEventArgs
    {
        public UInt16 Address { get; }
        public byte Value { get; }

        public MemoryReadEventArgs(UInt16 address, byte value)
        {
            Address = address;
            Value = value;
        }
    }
}
