﻿using System;
using static Memory.MemoryManager;

namespace Memory
{
    public interface IMemoryManager
    {
        byte[] Rom { get; }
        byte[] Ram { get; }
        byte[] VideoMemory { get; }
        bool AllowRomWrites { get; set; }

        event MemoryReadHandler MemoryRead;

        byte Read_8(int address);
        UInt16 Read_16(int address);
        void Write_8(int address, byte value);
        void Write_16(int address, UInt16 value);
    }
}
