﻿using System;

namespace Memory
{
    public class MemoryManager : IMemoryManager
    {
        public delegate void MemoryReadHandler(object sender, MemoryReadEventArgs e);

        public event MemoryReadHandler MemoryRead;

        public byte[] Rom { get; }

        public byte[] Ram { get; }

        public byte[] VideoMemory { get; }

        public bool AllowRomWrites { get; set; }

        public MemoryManager()
        {
            Rom = new byte[0x2000];
            Ram = new byte[0x400];
            VideoMemory = new byte[0x1C00];
            AllowRomWrites = false;
        }

        public byte Read_8(int address)
        {
            byte value = 0;
            if (address <= 0x1FFF)
            {
                value = Rom[address];
            }
            else if (address <= 0x23FF)
            {
                value = Ram[address - 0x2000];
            }
            else if (address <= 0x3FFF)
            {
                value = VideoMemory[address - 0x2400];
            }
            else
            {
                throw new InvalidOperationException($"Invalid memory read from {address:X4}");
            }
            OnMemoryRead(new MemoryReadEventArgs((UInt16)(address & 0xFFFF), value));
            return value;
        }

        public UInt16 Read_16(int address) => (UInt16)(Read_8(address) | (Read_8(address + 1) << 8));

        public void Write_8(int address, byte value)
        {
            if (address <= 0x1FFF)
            {
                if (AllowRomWrites)
                {
                    Rom[address] = value;
                }
            }
            else if (address <= 0x23FF)
            {
                Ram[address - 0x2000] = value;
            }
            else if (address <= 0x3FFF)
            {
                VideoMemory[address - 0x2400] = value;
            }
            //else
            //{
            //    throw new InvalidOperationException($"Invalid memory write of ${value:X2} to ${address:X4}");
            //}
        }

        public void Write_16(int address, UInt16 value)
        {
            Write_8(address, (byte)(value & 0xFF));
            Write_8(address + 1, (byte)((value >> 8) & 0xFF));
        }

        protected virtual void OnMemoryRead(MemoryReadEventArgs e)
        {
            MemoryRead?.Invoke(this, e);
        }
    }
}
