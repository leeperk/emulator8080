﻿using Ports;
using System;

namespace SpaceInvaders.ArcadeMachine
{
    public class ButtonManager : IButtonManager
    {
        #region Public

        public bool IsCoinButtonActive()
        {
            return IsKeyActive(ConsoleKey.C);
        }

        public bool IsPlayer1FireButtonActive()
        {
            return IsKeyActive(ConsoleKey.Spacebar);
        }

        public bool IsPlayer1LeftButtonActive()
        {
            return IsKeyActive(ConsoleKey.LeftArrow);
        }

        public bool IsPlayer1RightButtonActive()
        {
            return IsKeyActive(ConsoleKey.RightArrow);
        }

        public bool IsPlayer2FireButtonActive()
        {
            return IsKeyActive(ConsoleKey.Spacebar);
        }

        public bool IsPlayer2LeftButtonActive()
        {
            return IsKeyActive(ConsoleKey.LeftArrow);
        }

        public bool IsPlayer2RightButtonActive()
        {
            return IsKeyActive(ConsoleKey.RightArrow);
        }

        public bool IsPlayer1StartButtonActive()
        {
            return IsKeyActive(ConsoleKey.D1);
        }

        public bool IsPlayer2StartButtonActive()
        {
            return IsKeyActive(ConsoleKey.D2);
        }

        public bool IsRecordButtonActive()
        {
            return IsKeyActive(ConsoleKey.F1);
        }

        public bool IsStopRecordButtonActive()
        {
            return IsKeyActive(ConsoleKey.F2);
        }

        #endregion

        #region Private

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern short GetKeyState(int key);

        private bool IsKeyActive(ConsoleKey key)
        {

            return (GetKeyState((int)key) & 0x8000) != 0;
        }

        #endregion
    }
}
