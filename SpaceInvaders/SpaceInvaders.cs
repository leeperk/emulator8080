﻿using Audio;
using Ports;
using SpaceInvaders.ArcadeMachine;
using System.Configuration;

namespace SpaceInvaders
{
    public class SpaceInvaders
    {
        private static readonly string _pathForAudioFiles = ConfigurationManager.AppSettings["AudioFolder"];

        static void Main(string[] args)
        {
            using (IAudioManager audioManager = new AudioManager())
            {
                InitializeAudioManager(audioManager);

                ISpaceInvadersMachine spaceInvadersMachine = new Machine(new PortManager(new ButtonManager(), audioManager));
                spaceInvadersMachine.Start();
            }
        }

        private static void InitializeAudioManager(IAudioManager audioManager)
        {
            AddSampleToAudioManager(audioManager, SampleKeys.Ufo, true);
            AddSampleToAudioManager(audioManager, SampleKeys.Shot, false);
            AddSampleToAudioManager(audioManager, SampleKeys.Flash, false);
            AddSampleToAudioManager(audioManager, SampleKeys.InvaderDie, false);
            AddSampleToAudioManager(audioManager, SampleKeys.FleetMovement1, false);
            AddSampleToAudioManager(audioManager, SampleKeys.FleetMovement2, false);
            AddSampleToAudioManager(audioManager, SampleKeys.FleetMovement3, false);
            AddSampleToAudioManager(audioManager, SampleKeys.FleetMovement4, false);
            AddSampleToAudioManager(audioManager, SampleKeys.UfoHit, false);
        }

        private static void AddSampleToAudioManager(IAudioManager audioManager, string sampleKey, bool shouldLoop)
        {
            audioManager.AddSample(sampleKey, $"{ConfigurationManager.AppSettings[$"{sampleKey}Sample"]}", shouldLoop);
        }
    }
}
