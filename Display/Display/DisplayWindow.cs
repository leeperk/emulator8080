﻿using System.Windows.Forms;
using SharpDX;
using System;
using SharpDX.Mathematics.Interop;
using SharpDX.DXGI;
using D2D = SharpDX.Direct2D1;
using System.Threading;

namespace Display
{
    public class DisplayWindow : Form
    {
        #region Public

        public byte[] PixelData
        {
            get
            {
                return _pixelData;
            }
            set
            {
                while (_pixelDataLocked) ;
                _pixelData = value;
                Invalidate();
            }
        }
        public int RenderWidth { get; set; }
        public int RenderHeight { get; set; }
        public int RenderBitsPerPixel { get; set; }
        public int RenderStride => RenderWidth * RenderBitsPerPixel / 8;
        public bool Rotate90DegreesCounterClockwise { get; set; }

        public DisplayWindow(int width, int height)
        {
            Rotate90DegreesCounterClockwise = false;
            StartPosition = FormStartPosition.CenterScreen;
            ClientSize = new System.Drawing.Size(width, height);
            PixelData = new byte[RenderHeight * RenderStride];
            Load += GraphicsWindow_Load;
            Paint += GraphicsWindow_Paint;
            Resize += GraphicsWindow_Resize;
            FormClosing += GraphicsWindow_FormClosing;
            ResumeLayout(false);
        }

        public void RunInNewThread()
        {
            if (IsHandleCreated)
            {
                throw new InvalidOperationException("Form is already running.");
            }
            Thread thread = new Thread(state => Application.Run(state as Form));
            thread.SetApartmentState(ApartmentState.STA);
            thread.IsBackground = false;
            thread.Start(this);
        }

        #endregion

        #region Private

        private static Random _random = new Random();

        private D2D.WindowRenderTarget _target;
        private D2D.Factory _factory = new D2D.Factory();
        private SharpDX.DirectWrite.Factory _factoryWrite = new SharpDX.DirectWrite.Factory();
        private DateTime _frameStartTime;
        private int _frames = 0;
        private byte[] _pixelData;
        private bool _pixelDataLocked = false;

        private void GraphicsWindow_Load(object sender, EventArgs e)
        {
            D2D.RenderTargetProperties renderProp = new D2D.RenderTargetProperties()
            {
                DpiX = 0,
                DpiY = 0,
                MinLevel = D2D.FeatureLevel.Level_10,
                PixelFormat = new D2D.PixelFormat(SharpDX.DXGI.Format.R8G8B8A8_UNorm, D2D.AlphaMode.Ignore),
                Type = D2D.RenderTargetType.Hardware,
                Usage = D2D.RenderTargetUsage.None
            };

            D2D.HwndRenderTargetProperties winProp = new D2D.HwndRenderTargetProperties()
            {
                Hwnd = Handle,
                PixelSize = new Size2(ClientSize.Width, ClientSize.Height),
                PresentOptions = D2D.PresentOptions.Immediately
            };

            _target = new D2D.WindowRenderTarget(_factory, renderProp, winProp);
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque, true);
            _frameStartTime = DateTime.Now;
        }

        private void GraphicsWindow_Paint(object sender, PaintEventArgs e)
        {
            _frames++;
            if ((DateTime.Now - _frameStartTime).TotalMilliseconds >= 1000)
            {
                //Console.WriteLine(_frames);
                _frames = 0;
                _frameStartTime = DateTime.Now;
            }
            Draw();
        }

        private void GraphicsWindow_Resize(object sender, EventArgs e)
        {
            _target?.Resize(new Size2(ClientSize.Width, ClientSize.Height));
        }

        private void GraphicsWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            _target?.Dispose();
            _factory?.Dispose();
            _factoryWrite?.Dispose();

        }

        private void Draw()
        {
            _target.BeginDraw();

            _target.Clear(SharpDX.Color.CornflowerBlue);
            using (var bitmap = GenerateD2DBitmapFromData(_target))
            {
                _target.DrawBitmap(bitmap, new RawRectangleF(0, 0, ClientSize.Width, ClientSize.Height), 1.0f, D2D.BitmapInterpolationMode.NearestNeighbor);
            }
            _target.EndDraw();
        }

        private D2D.Bitmap GenerateD2DBitmapFromData(D2D.RenderTarget renderTarget)
        {
            var bitmapProperties = new D2D.BitmapProperties(new D2D.PixelFormat(Format.R8G8B8A8_UNorm, D2D.AlphaMode.Ignore));
            var size = new SharpDX.Size2(RenderWidth, RenderHeight);

            _pixelDataLocked = true;
            var bytes = ConvertDataToRGBXFormat();
            _pixelDataLocked = false;

            using (var tempStream = DataStream.Create(bytes, true, false))
            {
                return new D2D.Bitmap(renderTarget, size, tempStream, RenderWidth * 4, bitmapProperties);
            }
        }

        private byte[] ConvertDataToRGBXFormat()
        {
            if (Rotate90DegreesCounterClockwise)
            {
                return ConvertDataRotating90DegreesClockWise();
            }
            return ConvertDataWithoutAdjustingOrientation();
        }

        private byte[] ConvertDataWithoutAdjustingOrientation()
        {
            if (RenderBitsPerPixel == 32)
            {
                return PixelData;
            }
            if (RenderBitsPerPixel == 1)
            {
                var bytes = new byte[RenderWidth * 4 * RenderHeight];
                var bitMasks = new byte[] { 0b0000_0001, 0b0000_0010, 0b0000_0100, 0b0000_1000, 0b0001_0000, 0b0010_0000, 0b0100_0000, 0b1000_0000 };
                var bitNumber = 7;
                var dataIndex = 0;
                for (var index = 0; index < bytes.Length; index += 4)
                {
                    byte data = (byte)((PixelData[dataIndex] & bitMasks[bitNumber]) > 0 ? 255 : 0);
                    bytes[index + 0] = bytes[index + 1] = bytes[index + 2] = data;
                    if (--bitNumber < 0)
                    {
                        bitNumber = 7;
                        dataIndex++;
                    }
                }
                return bytes;
            }
            throw new InvalidOperationException($"BitsPerPixel of {RenderBitsPerPixel} is not supported in ConvertDataToRGBXFormat");
        }

        private byte[] ConvertDataRotating90DegreesClockWise()
        {
            if (RenderBitsPerPixel == 32)
            {
                return Rotate32BppPixelData90DegreesClockwise();
            }
            else if (RenderBitsPerPixel == 1)
            {
                return Rotate1BppPixelData90DegreesClockwise();
            }
            throw new InvalidOperationException($"BitsPerPixel of {RenderBitsPerPixel} is not supported in ConvertDataToRGBXFormat");
        }

        private byte[] Rotate32BppPixelData90DegreesClockwise()
        {
            const int outputBytesPerPixel = 4;
            var outputBytes = new byte[RenderWidth * outputBytesPerPixel * RenderHeight];
            var outputIndex = (RenderWidth - 1) * outputBytesPerPixel;
            var outputStride = RenderWidth * outputBytesPerPixel;
            for (var dataIndex = 0; dataIndex < PixelData.Length; dataIndex += 4)
            {
                outputBytes[outputIndex + 0] = PixelData[dataIndex + 0];
                outputBytes[outputIndex + 1] = PixelData[dataIndex + 1];
                outputBytes[outputIndex + 2] = PixelData[dataIndex + 2];
                outputBytes[outputIndex + 3] = PixelData[dataIndex + 3];
                outputIndex += outputStride;
                if (outputIndex > outputBytes.Length)
                {
                    outputIndex -= outputStride - 1;
                    --outputIndex;
                }
            }
            return outputBytes;
        }

        private byte[] Rotate1BppPixelData90DegreesClockwise()
        {
            const int outputBytesPerPixel = 4;
            var outputBytes = new byte[RenderWidth * outputBytesPerPixel * RenderHeight];
            var outputStride = RenderWidth * outputBytesPerPixel;
            var outputIndex = (RenderHeight - 1) * outputStride;
            var outputIndexAtWhichRenderingIsComplete = outputStride;
            var outputComplete = false;

            var bitMasks = new byte[] { 0b0000_0001, 0b0000_0010, 0b0000_0100, 0b0000_1000, 0b0001_0000, 0b0010_0000, 0b0100_0000, 0b1000_0000 };
            var bitNumber = 0;
            var dataIndex = 0;

            while (false == outputComplete)
            {
                byte data = (byte)((PixelData[dataIndex] & bitMasks[bitNumber]) > 0 ? 255 : 0);
                outputBytes[outputIndex + 0] = outputBytes[outputIndex + 1] = outputBytes[outputIndex + 2] = data;

                if (++bitNumber == 8)
                {
                    bitNumber = 0;
                    dataIndex++;

                }
                outputIndex -= outputStride;
                if (outputIndex < 0)
                {
                    outputIndex += (outputStride * RenderHeight) + outputBytesPerPixel;
                    if (outputIndex >= outputBytes.Length)
                    {
                        outputComplete = true;
                    }
                }
            }
            return outputBytes;
        }

        #endregion
    }
}
