﻿using Cpu8080;
using Cpu8080.Components;
using Ports;
using System;
using System.Configuration;
using System.IO;

namespace SpaceInvaders.ArcadeMachine
{
    public class DiagnosticMachine : Machine
    {
        public DiagnosticMachine(IPortManager portManager) : base(portManager)
        {
        }

        protected override void OnBeforeRomLoad()
        {
            Memory.AllowRomWrites = true;
            TestMemory();
        }

        protected override void LoadRomIntoMemory()
        {
            var fullPathForRomBinaries = Path.GetFullPath(PathForRomBinaries);
            LoadRomIntoMemory($"{fullPathForRomBinaries}\\cpudiag", 0x0100);
            FixUpRom();
        }

        protected override byte ProcessInstruction()
        {
            var cycles = base.ProcessInstruction();

            if (Cpu.State.PC == 0x0689)
            {
                Console.WriteLine();
                Console.WriteLine("CPU HAS FAILED!");
                Environment.Exit(1);
            }
            else if (Cpu.State.PC == 0x069B)
            {
                Console.WriteLine();
                Console.WriteLine("CPU IS OPERATIONAL");
                Environment.Exit(1);
            }
            return cycles;
        }

        private void FixUpRom()
        {
            Memory.Write_8(0, OpCodes.xc3_JMP_addr);
            Memory.Write_8(1, 0);
            Memory.Write_8(2, 1);

            Memory.Write_8(0x170, 0x7);

            Memory.Write_8(0x59c, OpCodes.xc3_JMP_addr);
            Memory.Write_8(0x59d, 0xc2);
            Memory.Write_8(0x59e, 0x05);
        }

        private void TestMemory()
        {
            byte testByte = 0;
            for(UInt16 address = 0; address <= 0x3FFF; address++)
            {
                Memory.Write_8(address, testByte++);
            }

            testByte = 0;
            for (UInt16 address = 0; address <= 0x3FFF; address++)
            {
                var value = Memory.Read_8(address);
                if (value != testByte)
                {
                    Console.WriteLine($"Memory Error! Read of {address:X4} resulted in {value} instead of {testByte}.");
                    Environment.Exit(1);
                }
                testByte++;
            }

            Console.WriteLine("Memory Test Success!");
        }

    }
}
