﻿using Cpu8080;
using Cpu8080.Components;
using Debugger;
using Display;
using Memory;
using Ports;
using System;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace SpaceInvaders.ArcadeMachine
{
    public class Machine : ISpaceInvadersMachine
    {
        private const int _screenWidth = 224;
        private const int _screenHeight = 256;

        protected Cpu Cpu;
        protected IMemoryManager Memory;
        protected DisplayWindow Display;
        protected Disassembler Disassembler;
        protected IPortManager PortManager;
        protected DebugTerminal DebugTerminal;

        protected virtual string PathForRomBinaries => ConfigurationManager.AppSettings["RomFolder"];

        private const long _ticksPerInterrupt = TimeSpan.TicksPerSecond / 60 / 2;
        private const long _cyclesPerSecond = 2000000;
        private const long _ticksPerCycle = TimeSpan.TicksPerSecond / _cyclesPerSecond;

        private bool _isDebugActive = false;
        private bool _isInInterrupt = false;
        private StringBuilder _debugBuffer;
        private Queue _debugQueue;

        public Machine(IPortManager portManager)
        {
            Memory = new MemoryManager();
            //Memory.MemoryRead += Memory_MemoryRead;
            PortManager = portManager;
            Cpu = new Cpu(Memory, portManager);
            Disassembler = new Disassembler();
            DebugTerminal = new DebugTerminal(Cpu, Memory);
            _debugQueue = new Queue(50);
            Display = new DisplayWindow(_screenWidth * 3, _screenHeight * 3)
            {
                RenderWidth = _screenWidth,
                RenderHeight = _screenHeight,
                RenderBitsPerPixel = 1,
                PixelData = new byte[_screenWidth * _screenHeight / 8],
                Rotate90DegreesCounterClockwise = true
            };
            Display.RunInNewThread();

            OnBeforeRomLoad();

            LoadRomIntoMemory();

        }

        protected virtual void OnBeforeRomLoad()
        {
        }

        protected virtual void LoadRomIntoMemory()
        {
            var fullPathForRomBinaries = Path.GetFullPath(PathForRomBinaries);
            LoadRomIntoMemory($"{fullPathForRomBinaries}\\invaders.h", 0x0000);
            LoadRomIntoMemory($"{fullPathForRomBinaries}\\invaders.g", 0x0800);
            LoadRomIntoMemory($"{fullPathForRomBinaries}\\invaders.f", 0x1000);
            LoadRomIntoMemory($"{fullPathForRomBinaries}\\invaders.e", 0x1800);
        }

        protected virtual void LoadRomIntoMemory(string pathAndFilenameForRomBinary, UInt16 startAddress)
        {
            var bytes = File.ReadAllBytes(pathAndFilenameForRomBinary);
            Array.Copy(bytes, 0, Memory.Rom, startAddress, bytes.Length);
        }

        protected virtual byte ProcessInstruction()
        {
            //if (Cpu.State.PC == 0x01A1 && false == _isDebugActive)
            //{
            //    _isDebugActive = true;
            //    _debugBuffer = new StringBuilder();
            //}
            //if (_isDebugActive)
            //{
            //    if (true == _isInInterrupt)
            //    {
            //        _debugBuffer.Append($"{Disassembler.GenerateStateForDisplay(Cpu.State, Memory)} | {Disassembler.GenerateDisassembledInstructionForDisplay(Memory, Cpu.State.PC)}{Environment.NewLine}");
            //    }
            //}
            //if (Cpu.State.PC == 0x01BE && _isDebugActive) // && PortManager.ReadPort(255) == 2)
            //{
            //    File.AppendAllText(@"D:\Temp\Dump.txt", _debugBuffer.ToString());
            //    File.AppendAllText(@"D:\Temp\Dump.txt", Environment.NewLine);
            //    _isDebugActive = false;
            //}

            try
            {
                //_debugQueue.Enqueue($"{Disassembler.GenerateStateForDisplay(Cpu.State, Memory)} | {Disassembler.GenerateDisassembledInstructionForDisplay(Memory, Cpu.State.PC)}{Environment.NewLine}");
                //if (_debugQueue.Count > 10)
                //{
                //    _debugQueue.Dequeue();
                //}
                //byte alienDeltaYrBefore = Memory.Read_8(0x2007);
                byte cycles = Cpu.ExecuteNextInstruction();
                //byte alienDeltaYrAfter = Memory.Read_8(0x2007);
                //if (alienDeltaYrBefore != alienDeltaYrAfter)
                //{
                //    while (_debugQueue.Count > 0)
                //    {
                //        File.AppendAllText(@"D:\Temp\Dump.txt", _debugQueue.Dequeue().ToString());
                //    }
                //}
                return cycles;
            }
            catch (Exception ex)
            {
                //while (_debugQueue.Count > 0)
                //{
                //    File.AppendAllText(@"D:\Temp\Dump.txt", _debugQueue.Dequeue().ToString());
                //}
                //if (_debugBuffer?.Length > 0)
                //{
                //    File.AppendAllText(@"D:\Temp\Dump.txt", _debugBuffer.ToString());
                //}
                //File.AppendAllText(@"D:\Temp\Dump.txt", Environment.NewLine);

                //File.AppendAllText(@"D:\Temp\Dump.txt", ex.ToString());
                throw;
            }
        }

        protected virtual byte GetDataFromPort(byte port)
        {
            return PortManager.ReadPort(port);
        }

        protected virtual void SendDataToPort(byte port, byte value)
        {
            PortManager.WritePort(port, value);
        }

        public void Start()
        {
            // Stopwatch.Elapsed is a TimeSpan
            // TimeSpan.TicksPerMillisecond == 10,000
            // TimeSpan.TicksPerMillisecond / 1000 == 10 == Ticks per microsecond (1 millionth of a second)
            // TimeSpan.TicksPerMillisecond / 1000 / 2 == 5 == Ticks per 8080 clock cycle (2 million per second).
            // 1/60 second == 1/60 * 1,000,000 = 16,666(.666...) microseconds
            // 16,666(.666...) microseconds * 2 cycles per microsecond = 33,333(.333...) cycles per 1/60 second
            // 33,333(.333...) / 2 = 16,666(.666...) cycles per interrupt
            // When it's time for an interrupt, we need to execute 16,666(.666) cycles worth of instructions and then do nothing until the next interrupt.

            var time = Stopwatch.StartNew();
            long nextInterruptAtTick = _ticksPerInterrupt * 2;
            byte nextInterruptNumber = 1;
            int frameCount = 0;
            long frameStartTicks = 0;
            long cyclesToBurn = (nextInterruptAtTick - time.Elapsed.Ticks) / _ticksPerCycle;
            long actualCyclesBurned = 0;
            long actualCyclesBurnedPerSecond = 0;

            do
            {
                var currentTicks = time.Elapsed.Ticks;

                if (currentTicks > nextInterruptAtTick)
                {
                    if (nextInterruptNumber == 1)
                    {
                        if ((currentTicks - frameStartTicks) >= TimeSpan.TicksPerSecond)
                        {
                            Console.WriteLine($"{frameCount} fps, {actualCyclesBurnedPerSecond} Hz");
                            frameCount = 0;
                            actualCyclesBurnedPerSecond = 0;
                            frameStartTicks = currentTicks;
                        }
                        else
                        {
                            frameCount++;
                        }
                    }
                    if (Cpu.State.InterruptsEnabled == 1)
                    {
                        Cpu.Interrupt(nextInterruptNumber);
                    }
                    _isInInterrupt = true;
                    if (++nextInterruptNumber == 3)
                    {
                        nextInterruptNumber = 1;
                    }
                    else
                    {
                        Display.PixelData = Memory.VideoMemory;
                    }
                    nextInterruptAtTick = currentTicks + _ticksPerInterrupt;

                    cyclesToBurn = (nextInterruptAtTick - time.Elapsed.Ticks) / _ticksPerCycle;
                    //Console.WriteLine($"actualCyclesBurned = {actualCyclesBurned}, cyclesToBurn = {cyclesToBurn}");
                    actualCyclesBurnedPerSecond += actualCyclesBurned;
                    actualCyclesBurned = 0;
                }
                if (Cpu.State.PC == 0x0087)
                {
                    _isInInterrupt = false;
                }

                //Console.WriteLine($"actualCyclesBurned = {actualCyclesBurned}, cyclesToBurn = {cyclesToBurn}, currentTicks = {currentTicks}, nextInterruptAtTick = {nextInterruptAtTick}, {Cpu.State.InterruptsEnabled}");
                if (actualCyclesBurned <= cyclesToBurn)
                {
                    actualCyclesBurned += ProcessInstruction();
                }
            } while (true);
        }
        private void Memory_MemoryRead(object sender, MemoryReadEventArgs e)
        {
            if (e.Address == 0x1C00)
            {
                _isDebugActive = true;
            }
        }
    }
}
