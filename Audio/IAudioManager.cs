﻿using System;

namespace Audio
{
    public interface IAudioManager : IDisposable
    {
        void AddSample(string sampleKey, string pathAndFilenameOfSample, bool shouldLoop);
        void PlayLooping(string sampleKey);
        void PlayOnce(string sampleKey);
        void Stop(string sampleKey);
    }
}