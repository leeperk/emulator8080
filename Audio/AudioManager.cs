﻿using SharpDX.Multimedia;
using SharpDX.XAudio2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;

namespace Audio
{
    public class AudioManager : IAudioManager
    {
        private readonly XAudio2 _xAudio;
        private readonly MasteringVoice _masteringVoice;
        private readonly Dictionary<string, (BackgroundWorker Worker, string PathAndFilenameOfSample)> _samplePlayers;

        public AudioManager()
        {
            _xAudio = new XAudio2();
            _xAudio.StartEngine();
            _masteringVoice = new MasteringVoice(_xAudio);
            _masteringVoice.SetVolume(1);
            _samplePlayers = new Dictionary<string, (BackgroundWorker, string)>();
        }

        public void AddSample(string sampleKey, string pathAndFilenameOfSample, bool shouldLoop)
        {
            var backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.RunWorkerCompleted += (sender, e) => BackgroundWorker_RunWorkerCompleted(sampleKey);
            backgroundWorker.DoWork += (sender, args) =>
            {
                var stream = new SoundStream(File.OpenRead(pathAndFilenameOfSample));
                var waveFormat = stream.Format;
                var buffer = new AudioBuffer
                {
                    Stream = stream.ToDataStream(),
                    AudioBytes = (int)stream.Length,
                    Flags = BufferFlags.EndOfStream,
                    LoopCount = shouldLoop ? AudioBuffer.LoopInfinite : 0
                };
                stream.Close();

                var sourceVoice = new SourceVoice(_xAudio, waveFormat, true);
                sourceVoice.SubmitSourceBuffer(buffer, stream.DecodedPacketsInfo);
                sourceVoice.Start();

                while (sourceVoice.State.BuffersQueued > 0 && args.Cancel == false)
                {
                    if (backgroundWorker.CancellationPending)
                    {
                        args.Cancel = true;
                    }
                    Thread.Sleep(1);
                }

                sourceVoice.DestroyVoice();
                sourceVoice.Dispose();
                buffer.Stream.Dispose();
            };
            if (_samplePlayers.ContainsKey(sampleKey))
            {
                _samplePlayers.Remove(sampleKey);
            }
            _samplePlayers.Add(sampleKey, (backgroundWorker, pathAndFilenameOfSample));
        }

        private void BackgroundWorker_RunWorkerCompleted(string sampleKey)
        {
            Console.WriteLine($"Sample '{sampleKey}' has finished.");
        }

        public void PlayOnce(string sampleKey)
        {
            if (false == _samplePlayers.ContainsKey(sampleKey))
            {
                return;
            }
            AddSample(sampleKey, _samplePlayers[sampleKey].PathAndFilenameOfSample, false);
            var samplePlayer = _samplePlayers[sampleKey].Worker;

            if (samplePlayer.IsBusy)
            {
                samplePlayer.CancelAsync();
                while (samplePlayer.IsBusy) { }
            }
            samplePlayer.RunWorkerAsync();
        }

        public void PlayLooping(string sampleKey)
        {
            if (false == _samplePlayers.ContainsKey(sampleKey))
            {
                return;
            }
            AddSample(sampleKey, _samplePlayers[sampleKey].PathAndFilenameOfSample, true);
            var samplePlayer = _samplePlayers[sampleKey].Worker;

            if (samplePlayer.IsBusy)
            {
                samplePlayer.CancelAsync();
                while (samplePlayer.IsBusy) { }
            }
            samplePlayer.RunWorkerAsync();
        }

        public void Stop(string sampleKey)
        {
            if (false == _samplePlayers.ContainsKey(sampleKey))
            {
                return;
            }
            var samplePlayer = _samplePlayers[sampleKey].Worker;
            if (samplePlayer.IsBusy)
            {
                samplePlayer.CancelAsync();
            }
        }

        public void Dispose()
        {
            _masteringVoice?.Dispose();
            _xAudio?.Dispose();
        }
    }
}
