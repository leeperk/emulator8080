﻿namespace Audio
{
    public static class SampleKeys
    {
        public const string Ufo = "Ufo";
        public const string Shot = "Shot";
        public const string Flash = "Flash";
        public const string InvaderDie = "InvaderDie";
        public const string FleetMovement1 = "FleetMovement1";
        public const string FleetMovement2 = "FleetMovement2";
        public const string FleetMovement3 = "FleetMovement3";
        public const string FleetMovement4 = "FleetMovement4";
        public const string UfoHit = "UfoHit";
    }
}
