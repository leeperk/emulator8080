﻿namespace Audio
{
    public static class SampleBitMask
    {
        public const byte Ufo = 0b0000_0001;
        public const byte Shot = 0b0000_0010;
        public const byte Flash = 0b0000_0100;
        public const byte InvaderDie = 0b0000_1000;
        public const byte FleetMovement1 = 0b0000_0001;
        public const byte FleetMovement2 = 0b0000_0010;
        public const byte FleetMovement3 = 0b0000_0100;
        public const byte FleetMovement4 = 0b0000_1000;
        public const byte UfoHit = 0b0001_0000;
    }
}
