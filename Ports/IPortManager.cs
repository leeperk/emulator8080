﻿namespace Ports
{
    public interface IPortManager
    {
        byte ReadPort(byte port);
        void WritePort(byte port, byte value);
    }
}
