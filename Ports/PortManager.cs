﻿using Audio;
using System;

namespace Ports
{
    public class PortManager : IPortManager
    {
        #region Public

        public PortManager(IButtonManager buttonManager, IAudioManager audioManager)
        {
            _port1Bits = 0b0000_1000;
            _port2Bits = 0b0000_1000;
            _buttonManager = buttonManager;
            _audioManager = audioManager;
        }

        public byte ReadPort(byte port)
        {
            //Console.Write($"Read {port} = ");
            byte value = 0;
            switch (port)
            {
                case 0:
                    value = 1;
                    break;
                case 1:
                    UpdatePort1Bits();
                    value = _port1Bits;
                    break;
                case 2:
                    UpdatePort2Bits();
                    value = _port2Bits;
                    break;
                case 3:
                    value = (byte)(((_shift1 << 8 | _shift0) >> (8 - _shiftOffset)) & 0xff);
                    break;
                case 255:
                    if (_buttonManager.IsRecordButtonActive())
                    {
                        value = 1;
                    }
                    else if (_buttonManager.IsStopRecordButtonActive())
                    {
                        value = 2;
                    }
                    break;
            }
            //Console.WriteLine($"{value}");
            return value;
        }

        public void WritePort(byte port, byte value)
        {
            //Console.WriteLine($"Write {port}, {value}");
            switch (port)
            {
                case 2:
                    _shiftOffset = (byte)(value & 0x7);
                    break;
                case 3:
                    {
                        AdjustAudio(port, value);
                        _port3Bits = value;
                        break;
                    }
                case 4:
                    _shift0 = _shift1;
                    _shift1 = value;
                    break;
                case 5:
                    {
                        AdjustAudio(port, value);
                        _port5Bits = value;
                        break;
                    }
            }
        }

        #endregion

        #region Private

        private const byte _coinBitMask         = 0b0000_0001;
        private const byte _player2StartBitMask = 0b0000_0010;
        private const byte _player1StartBitMask = 0b0000_0100;
        private const byte _playerFireBitMask  = 0b0001_0000;
        private const byte _playerLeftBitMask  = 0b0010_0000;
        private const byte _playerRightBitMask = 0b0100_0000;

        private readonly IButtonManager _buttonManager;
        public readonly IAudioManager _audioManager;

        private byte _port1Bits;
        private byte _port2Bits;
        private byte _port3Bits;
        private byte _port5Bits;
        private byte _shift0;
        private byte _shift1;
        private byte _shiftOffset;

        private void UpdatePort1Bits()
        {
            ProcessPort1Button(_buttonManager.IsCoinButtonActive(), _coinBitMask);
            ProcessPort1Button(_buttonManager.IsPlayer2StartButtonActive(), _player2StartBitMask);
            ProcessPort1Button(_buttonManager.IsPlayer1StartButtonActive(), _player1StartBitMask);
            ProcessPort1Button(_buttonManager.IsPlayer1FireButtonActive(), _playerFireBitMask);
            ProcessPort1Button(_buttonManager.IsPlayer1LeftButtonActive(), _playerLeftBitMask);
            ProcessPort1Button(_buttonManager.IsPlayer1RightButtonActive(), _playerRightBitMask);
        }

        private void UpdatePort2Bits()
        {
            ProcessPort2Button(_buttonManager.IsPlayer2FireButtonActive(), _playerFireBitMask);
            ProcessPort2Button(_buttonManager.IsPlayer2LeftButtonActive(), _playerLeftBitMask);
            ProcessPort2Button(_buttonManager.IsPlayer2RightButtonActive(), _playerRightBitMask);
        }

        private void ProcessPort1Button(bool isButtonActive, byte bitMask)
        {
            if (isButtonActive)
            {
                _port1Bits |= bitMask;
            }
            else
            {
                _port1Bits &= (byte)~bitMask;
            }
        }

        private void ProcessPort2Button(bool isButtonActive, byte bitMask)
        {
            if (isButtonActive)
            {
                _port2Bits |= bitMask;
            }
            else
            {
                _port2Bits &= (byte)~bitMask;
            }
        }

        private void AdjustAudio(byte port, byte value)
        {
            if (port == 3)
            {
                PlayOrStopSampleIfRequested(_port3Bits, value, SampleBitMask.Ufo, SampleKeys.Ufo, true);
                PlayOrStopSampleIfRequested(_port3Bits, value, SampleBitMask.Shot, SampleKeys.Shot, false);
                PlayOrStopSampleIfRequested(_port3Bits, value, SampleBitMask.Flash, SampleKeys.Flash, false);
                PlayOrStopSampleIfRequested(_port3Bits, value, SampleBitMask.InvaderDie, SampleKeys.InvaderDie, false);
            }
            else
            {
                PlayOrStopSampleIfRequested(_port5Bits, value, SampleBitMask.FleetMovement1, SampleKeys.FleetMovement1, false);
                PlayOrStopSampleIfRequested(_port5Bits, value, SampleBitMask.FleetMovement2, SampleKeys.FleetMovement2, false);
                PlayOrStopSampleIfRequested(_port5Bits, value, SampleBitMask.FleetMovement3, SampleKeys.FleetMovement3, false);
                PlayOrStopSampleIfRequested(_port5Bits, value, SampleBitMask.FleetMovement4, SampleKeys.FleetMovement4, false);
                PlayOrStopSampleIfRequested(_port5Bits, value, SampleBitMask.UfoHit, SampleKeys.UfoHit, false);
            }
        }

        private void PlayOrStopSampleIfRequested(byte previousValue, byte currentValue, byte bitMask, string sampleKey, bool shouldLoop)
        {
            if (HasAudioRequestBeganSincePreviousCheck(previousValue, currentValue, bitMask))
            {
                (shouldLoop ? (Action<string>)_audioManager.PlayLooping : _audioManager.PlayOnce)(sampleKey);
            }
            else if (HasAudioRequestEndedSincePreviousCheck(previousValue, currentValue, bitMask))
            {
                _audioManager.Stop(sampleKey);
            }
        }

        private bool HasAudioRequestBeganSincePreviousCheck(byte previousValue, byte currentValue, byte bitMask)
        {
            return (previousValue & bitMask) == 0 && (currentValue & bitMask) > 0;
        }

        private bool HasAudioRequestEndedSincePreviousCheck(byte previousValue, byte currentValue, byte bitMask)
        {
            return (previousValue & bitMask) > 0 && (currentValue & bitMask) == 0;
        }

        #endregion
    }
}
