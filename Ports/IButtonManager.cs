﻿namespace Ports
{
    public interface IButtonManager
    {
        bool IsCoinButtonActive();
        bool IsPlayer2StartButtonActive();
        bool IsPlayer1StartButtonActive();
        bool IsPlayer1FireButtonActive();
        bool IsPlayer1LeftButtonActive();
        bool IsPlayer1RightButtonActive();
        bool IsPlayer2FireButtonActive();
        bool IsPlayer2LeftButtonActive();
        bool IsPlayer2RightButtonActive();
        bool IsRecordButtonActive();
        bool IsStopRecordButtonActive();
    }
}
